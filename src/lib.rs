use std::io::{BufReader, BufWriter, Read, Write};
use std::fs::File;

pub fn xor_crypt(in_filepath: String, out_filepath: String, key: String) {
    let infile = File::open(in_filepath).expect("File open error");
    let mut reader = BufReader::new(infile);
    let outfile = File::create(out_filepath).expect("File creation error");
    let mut writer = BufWriter::new(outfile);
    let mut buf: [u8; 1024] = [0; 1024];
    reader.read(&mut buf);
    let obuf = xor(&buf, key);
    writer.write(obuf.as_slice());
}

fn xor(buffer: &[u8], key: String) -> Vec<u8> {
    let mut outbuf = vec![];
    let keybuf = key.as_bytes();
    for b in buffer {
        let mut rb = *b;
        for kb in keybuf {
            rb = rb ^ *kb;
        }
        outbuf.push(rb);
    }
    return outbuf;
}

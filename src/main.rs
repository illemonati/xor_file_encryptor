#[macro_use]
extern crate clap;

use xor_file_encryptor::xor_crypt;

#[derive(Clap)]
#[clap(version = "1.0", author = "illemonati")]
struct Opts {
    #[clap(short = "i", long = "infile")]
    infile: String,
    #[clap(short = "o", long = "outfile")]
    outfile: String,
    #[clap(short = "k", long = "key")]
    key: String,
    #[clap(short = "x", long = "xor")]
    xor: bool,

}

fn main() {
    let opts: Opts = Opts::parse();
    if opts.xor {
        xor_crypt(opts.infile, opts.outfile, opts.key);
    }
}



